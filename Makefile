CC:=x86_64-elf-gcc
CFLAGS:=-Wall -Wextra -std=c99

LD:=x86_64-elf-ld
LDFLAGS:=-T src/linker.ld -nostdlib -nostartfiles -nodefaultlibs

AS:=nasm -f bin
ASFLAGS:

ISO:=genisoimage
ISOFLAGS:=\
-iso-level 3 \
-no-emul-boot \

BOOT_OBJS:=\
src/eltorito.o \

iso: $(BOOT_OBJS)
	dd if=src/eltorito.o of=sysroot/boot.img
	$(ISO) $(ISOFLAGS) -b boot.img -hide boot.img -o boot.iso sysroot/
	rm src/eltorito.o
	echo "qemu-system-x86_64 -cdrom boot.iso" > run-qemu
	chmod +x run-qemu

%.o: %.asm
	$(AS) $(ASFLAGS) $< -o $@

clean:
	rm -f src/*.o
