[BITS 16]
ALIGN 4
[ORG 0x7C00]
boot:
    ; Zero segment registers
    xor ax, ax
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
    mov sp, stack_top
    cld

    call vga_clear
    
    ; Check for CPUID support
    pushfd
    pop  eax
    mov  ecx, eax
    xor  eax, 1 << 21
    push eax
    popfd
    pushfd
    pop  eax
    xor  eax, ecx
    shr  eax, 21
    and  eax, 1
    push ecx
    popfd
    cmp  ax, 0
    je   .no_cpuid
    mov  si, str_cpuid
    call io_puts

    ; Check for long mode support
    mov  eax, 0x80000000
    cpuid
    cmp  eax, 0x80000001
    jb   .no_longmode
    mov  eax, 0x80000001
    cpuid
    test edx, 1 << 29
    jz   .no_longmode
    mov  si, str_longmode
    call io_puts

    ; Enable A20 line
    call a20_enable
    cmp  ax, 0
    je   .no_a20
    mov  si, str_a20
    call io_puts

    ; Zero page table memory
    mov  edi, 0x1000
    mov  cr3, edi
    xor  eax, eax
    mov  ecx, 0x1000
    rep  stosd
    mov  edi, cr3

    ; Initialize page tables
    mov  dword [edi], 0x2003
    add  edi, 0x1000
    mov  dword [edi], 0x3003
    add  edi, 0x1000
    mov  dword [edi], 0x4003
    add  edi, 0x1000
    mov  ebx, 0x00000003
    mov  ecx, 512
    .ptable_loop:
        mov  dword [edi], ebx
        add  ebx, 0x1000
        add  edi, 8
    loop .ptable_loop

    mov eax, cr4
    mov eax, 1 << 5
    mov cr4, eax

    mov ecx, 0xC0000080
    rdmsr
    or  eax, 1 << 8
    wrmsr

    mov ebx, cr0
    or  ebx, 1 << 0
    or  ebx, 1 << 31
    mov cr0, ebx

    lgdt [gdt.pointer]
    jmp gdt.code:boot64

    .no_cpuid:
        mov  si, str_no_cpuid
        call io_puts
        jmp  .hang

    .no_longmode:
        mov  si, str_no_longmode
        call io_puts
        jmp  .hang

    .no_a20:
        mov  si, str_no_a20
        call io_puts
        jmp  .hang

    .hang:
        cli
        hlt
        jmp .hang

a20_check:
    pushf
    push ds
    push es
    push di
    push si

    cli

    xor ax, ax
    mov es, ax ; es = 0x0000

    not ax
    mov ds, ax ; ds = 0xFFFF

    mov di, 0x0500
    mov si, 0x0510

    mov al, byte [es:di]
    push ax

    mov al, byte [ds:si]
    push ax

    mov byte[es:di], 0x00
    mov byte[ds:si], 0xFF
    cmp byte[es:di], 0xFF

    pop ax
    mov byte [ds:si], al

    pop ax
    mov byte [es:di], al

    mov ax, 0x0000
    je .exit

    mov ax, 0x0001

    .exit:
        pop si
        pop di
        pop es
        pop ds
        popf

        ret

; ax = 1 if successful, 0 otherwise
a20_enable:
    pushf

    ; Who knows, we might get lucky
    call a20_check
    cmp ax, 1
    je .exit

    ; BIOS method
    mov ax, 0x2401
    int 0x15

    call a20_check
    cmp ax, 1
    je .exit

    ; Fast A20 gate (System control port A) method
    in   al, 0x92
    test al, 2
    jnz .after
    or   al, 2
    and  al, 0xFE
    out  0x92, al
    
    .after:
        call a20_check
        cmp ax, 1
        je .exit

    mov ax, 0

    .exit:
        popf
        ret

vga_clear:
    pushf
    pusha

    sti

    ; Clear the screen
    mov ax, 0x0700
    mov bh, 0x07
    mov cx, 0x0000
    mov dx, 0x184F
    int 0x10

    ; Move the cursor to (0,0)
    mov ax, 0x0200
    mov bh, 0x00
    mov dx, 0x0000
    int 0x10

    popa
    popf
    ret

io_puts:
    lodsb
    or  al, al
    jz  .return
    mov ah, 0x0E
    int 0x10
    jmp io_puts
    .return:
        ret

    %include "src/boot64.asm"

section .rodata
rodata:
    gdt:
        .null: equ $ - gdt
            dw 0, 0
            db 0, 0, 0, 0
        .code: equ $ - gdt
            dw 0, 0
            db 0, 10011000b, 00100000b, 0
        .data: equ $ - gdt
            dw 0, 0
            db 0, 10010000b, 00000000b, 0
        .pointer:
            dw $ - gdt - 1
            dq gdt

    str_cpuid       db "CPUID available.",                     0x0D, 0x0A, 0x00
    str_no_cpuid    db "CPUID not available.",                 0x0D, 0x0A, 0x00

    str_longmode    db "Long mode available.",                 0x0D, 0x0A, 0x00
    str_no_longmode db "This CPU does not support long mode.", 0x0D, 0x0A, 0x00

    str_a20         db "A20 line enabled.",                    0x0D, 0x0A, 0x00
    str_no_a20      db "A20 line could not be enabled.",       0x0D, 0x0A, 0x00

    str_jumped64    db "Successfully jumped to 64-bit mode.",  0x0D, 0x0A, 0x00

section .bss
bss:
    stack_bottom:
        resb 0x400
    stack_top:

    idt:
        resw 1
        resd 1
