[BITS 16]

section .text
start:
    jmp 0x0000:boot
    times 8 - ($ - $$) db 0

    iso_primary_volume_descriptor dd 0x00000000
    iso_boot_file_addr            dd 0x00000000
    iso_boot_file_length          dd 0x00000000
    iso_checksum                  dd 0x00000000
    iso_reserved                  times 40 db 0x00

%include "src/boot16.asm"
