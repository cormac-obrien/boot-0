boot64:
    [BITS 64]
    lidt[idt]

    call vga64_clear

    xchg bx, bx

    mov  edi, 0x000B8000
    mov  si, str_cpuid
    call vga64_puts
    call vga64_puts

    .halt:
        cli
        hlt
        jmp .halt

vga64_puts:
    lodsb

    or al, al
    jz .return

    mov bl, al
    mov bh, 0x17
    call vga64_putchar

    jmp vga64_puts

    .return:
        ret

vga64_putchar:
    pushfq
    push rax
    push rbx
    push rcx
    push rdx

    cmp bl, 0x0A
    je .newline

    cmp bl, 0x0D
    je .return

    jmp .default

    .newline:
        mov edx, 0
        mov eax, edi
        sub eax, 0x000B8000
        mov ebx, 160
        div ebx
        sub edi, edx
        add edi, 160
        jmp .return
    .default:
        mov dword [edi], ebx
        add edi, 2
    .return:
        mov ebx, edi
        pop rdx
        pop rcx
        pop rbx
        pop rax
        popfq
        ret

vga64_clear:
    pushfq
    push rbx
    push rcx

    mov edi, 0x000B8000
    mov ecx, 2000
    .loop:
        mov ebx, (0x02 << 8) | ' '
        mov dword[edi], ebx
        add edi, 2
    loop .loop

    pop rcx
    pop rbx
    popfq
    ret
